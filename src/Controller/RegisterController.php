<?php

namespace App\Controller;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use Symfony\Component\HttpFoundation\JsonResponse;


class RegisterController extends AbstractController
{
    /**
     * @Route("/register", name="register")
     */
    public function register( Request $request, UserPasswordEncoderInterface $passEncoder){
        $form = $this->createFormBuilder()
        ->add('username')
        ->add('password', RepeatedType::class,[
            'type' => PasswordType::class,
            'required' => true,
            'first_options' => ['label' => 'Password'],
            'second_options' => ['label' => 'Confirm Password']

        ])

        ->add('register', SubmitType::class,[
            'attr' =>[
                'class' =>'btn btn-success float-right'
            ]
        ])
        ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted()){
           
            $data = $form->getData();
            $user = new User();
            $user->setUsername( $data['username']);
       

            $user->setPassword(
                $passEncoder->encodePassword($user, $data['password'])
            );
          
           $em = $this->getDoctrine() -> getManager();
           $em->persist($user);
           $em->flush();
           return $this->redirect($this->generateUrl('app_login'));
                
        }
        
    
        return $this->render('register/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/api/login", name="api_login", methods="POST")
     */
    public function Login( Request $request, UserPasswordEncoderInterface $passEncoder){
       
            // 1 lire les parametres reçu
            $username = $request->request->get('username');
            $password = $request->request->get('password');
           
            // 2 rechercher l'utilisateur dans la table user 
            
            $em = $this->getDoctrine() -> getManager();

            $user = $em->getRepository(User::class)->findOneBy(array('username'=>$username));
            if($user!=null){
                // tester si le mdp est correcte
                $encoded = $passEncoder->encodePassword($user, $password);
                dump($encoded);
                dump($user->getPassword());
                die();
                
                if(true){
                    return new JsonResponse(['status'=>"success","user"=>["username"=>$user->getUsername()]],200);
                }

                return new JsonResponse(['status'=>"failure"],401);
            }else{
                // retourner erreur
                return new JsonResponse(['status'=>$username],404);
            }
            
            
    }
}