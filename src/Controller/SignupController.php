<?php

namespace App\Controller;

use App\Entity\Signup;
use App\Form\SignupType;
use App\Repository\SignupRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/signup")
 */
class SignupController extends AbstractController
{
    /**
     * @Route("/", name="signup_index", methods={"GET"})
     */
    public function index(SignupRepository $signupRepository): Response
    {
        return $this->render('signup/index.html.twig', [
            'signups' => $signupRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="signup_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $signup = new Signup();
        $form = $this->createForm(SignupType::class, $signup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($signup);
            $entityManager->flush();

            return $this->redirectToRoute('signup_index');
        }

        return $this->render('signup/new.html.twig', [
            'signup' => $signup,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="signup_show", methods={"GET"})
     */
    public function show(Signup $signup): Response
    {
        return $this->render('signup/show.html.twig', [
            'signup' => $signup,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="signup_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Signup $signup): Response
    {
        $form = $this->createForm(SignupType::class, $signup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('signup_index');
        }

        return $this->render('signup/edit.html.twig', [
            'signup' => $signup,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="signup_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Signup $signup): Response
    {
        if ($this->isCsrfTokenValid('delete'.$signup->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($signup);
            $entityManager->flush();
        }

        return $this->redirectToRoute('signup_index');
    }
}
