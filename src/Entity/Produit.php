<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\ProduitRepository")
 * @Vich\Uploadable()
 */
class Produit
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    

    /**
     * 
     * @Vich\UploadableField(mapping="produits_images",fileNameProperty="produitfilename")
     *  VichFileType::class
     */

    private $imageProduit;
  
     /**
     * @ORM\Column(type="string")
     *
     * @var string|null
     */

    private $produitfilename;

     /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTimeInterface|null
     */
    private $updatedAt;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $prix;

    /**
     * @ORM\Column(type="integer")
     */
    private $stock;

  
  

   
    public function __construct()
    {
       
        $this->updateAt = new \Datetime();
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setStock(int $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProduitfilename(): ?string
    {
        return $this->produitfilename;
    }

    /**
     * @param string|null $produitfilename
     * @return Produit
     */
    public function setProduitfilename(?string $produitfilename): Produit
    {
        $this->produitfilename = $produitfilename;
        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageProduit(): ?File
    {
        return $this->imageProduit;
    }

    /**
     * @param File|null $imageProduit
     * @return Produit
     */
    public function setImageProduit(?File $imageProduit): Produit
    {
        $this->imageProduit = $imageProduit;
        if($this -> imageProduit instanceof UploadedFile ){
            $this ->updated_at = new \DateTime('now');
        }

        return $this;
    }


}
