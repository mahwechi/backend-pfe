<?php

namespace App\Entity;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


/**
 * @ApiResource
 * @ORM\Entity(repositoryClass="App\Repository\RecettesRepository")
 * @Vich\Uploadable()

 */
class Recettes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string|null
     * @ORM\Column(type="string",length=255)
     */

    private $recettefilename;

    /**
     * @var File|null
     * @Vich\UploadableField(mapping="recettes_images",fileNameProperty="recettefilename")
     */

    private $imageRecette;


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=800)
     */
    private $ingredients;

    /**
     * @ORM\Column(type="string", length=800)
     */
    private $preparations;

     /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     *
     */
    private $updated_at;
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="please upload image")
     * @Assert\File(mimeTypes={"image/jpeg"})
     */
    private $image;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getIngredients(): ?string
    {
        return $this->ingredients;
    }

    public function setIngredients(string $ingredients): self
    {
        $this->ingredients = $ingredients;

        return $this;
    }

    public function getPreparations(): ?string
    {
        return $this->preparations;
    }

    public function setPreparations(string $preparations): self
    {
        $this->preparations = $preparations;

        return $this;
    }

   

     /**
     * @return string|null
     */
    public function getRecettefilename(): ?string
    {
        return $this->recettefilename;
    }

    /**
     * @param string|null $recettefilename
     * @return Recettes
     */
    public function setRecettefilename(?string $recettefilename): Recettes
    {
        $this->recettefilename = $recettefilename;
        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageRecette(): ?File
    {
        return $this->imageRecette;
     
    }

    /**
     * @param File|null $imageRecette
     * @return Recettes
     */
    public function setImageRecette(?File $imageRecette): Recettes
    {
        $this->imageRecette = $imageRecette;
        if($this -> imageRecette instanceof UploadedFile ){
            $this ->updated_at = new \DateTime('now');
        }
        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }




}
